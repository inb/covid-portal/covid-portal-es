FROM klakegg/hugo:0.81.0-ext-alpine-onbuild AS hugo

FROM nginx:1.18-alpine
COPY --from=hugo /target /usr/share/nginx/html