---
title: Inicio
---

El **portal español sobre datos de COVID-19** proveé información, guías, herramientas y servicios para ayudar a los investigadores a utilizar las infraestructuras españolas y europeas para compartir datos, en particular el [Portal europeo sobre datos de COVID-19](https://covid19dataportal.org). Para aquellos interesados en grandes proyectos españoles en curso, hemos reunido una [lista de proyectos financiados por grandes agencias de financiación](/research-projects).
