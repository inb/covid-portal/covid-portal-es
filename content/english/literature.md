---
title: Literature
menu:
  main:
    name: Literature
    weight: 3
type: literature
---

This section presents a list of the latest published scientific journal articles and preprints on COVID-19 and SARS-CoV-2 where at least one author has a Spanish affiliation. Items have been fetched from an automatic daily search from [Europe PMC](https://europepmc.org/) completed with other elements manually curated and uploaded from researchers.

There are filters at your disposal to navigate the list, i.e. publications with acknowledged funding to the “Fondo COVID19” extraordinary funds. Note that some articles have additional available data that have been curated manually and as such may not be exhaustive.

You can help us enriching this section by adding new papers not listed below or available associated data to existing ones (datasets, code repositories...) by [filling in this formulaire](https://bsc3.typeform.com/to/yFGM8nUc). Please make sure that the information is not already in the table below and that the publication has at least one author affiliated in Spain.
