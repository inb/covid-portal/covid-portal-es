---
title: Research projects
menu:
  main:
    name: Research projects
    weight: 4
type: research-projects
---

This section lists COVID-19 and SARS-CoV-2 funded research projects that contain at least one researcher, funding agency or research institution with Spanish affiliation or that the object of study is the Spanish population. Relevant information, such as title, description, principal investigator, beneficiary institution, funder, and primary WHO research area, for each project is available in the list. Additionally, a search box and filters are at your disposal to navigate the list, i.e. by funder or projects included on specific national call “Registro estatal de COVID19”. The table can be sorted by project title or reference ID, if any.

Projects are semi-automatic-daily fetched from the [COVID-19 Research Project Tracker](https://ukcdr.org.uk/data-tool/covid-19-research-project-tracker-by-ukcdr-glopid-r/) gathered by the UK Collaborative on Development Research ([UKCDR](https://www.ukcdr.org.uk/)). This is a live database of funded research projects worldwide related to the COVID-19 pandemic, as part of the **[COVID CIRCLE](http://www.ukcdr.org.uk/covid-circle/)** initiative. By providing an overview of research projects mapped against the priorities identified in the [WHO Coordinated Global Research Roadmap: 2019 Novel Coronavirus](https://www.who.int/publications/m/item/a-coordinated-global-research-roadmap), they support funders and researchers to deliver a more effective and coherent global research response.

**Projects granted by ISCIII COVID-19 funds and other projects adhered to the COVID-19 ISCIII Registry** are also part of this list. This extraordinary fund aims to promote knowledge about SARS-CoV-2 and COVID-19 disease and its impact on infected people, with the aim of contributing to efficient patient treatment and/or public health preparedness and response. The expected impact is to:

Contribute to improving the treatment of the disease in the current pandemic.
Contribute to improving the diagnosis and clinical management of patients infected by SARS-CoV-2.
Contribute to designing, developing and implementing public health measures to respond effectively to the ongoing SARS-CoV-2 epidemic.

You can help us enrich this section by notifying other research projects not listed below by sending an email to [covid19-dataportal@bsc.es](mailto:covid19-dataportal@bsc.es).
