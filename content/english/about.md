---
title: About
url: about
---

<div class="container">

## Introduction

The **Spanish COVID-19 Data Portal** provides information, guidelines, tools and services to support researchers in utilising Spanish and European infrastructures for data sharing, in particular the [European COVID-19 Data Portal](https://covid19dataportal.org/). For those interested in ongoing, large research projects in Spain we have compiled a [list of projects funded by major funding agencies](/research-projects). It is regularly updated with new services, tools and data. Suggestions, corrections or other comments are most welcome.

This website is a fork of the [Swedish COVID-19 Data Portal](https://www.covid19dataportal.se/). It has been built using [Hugo](https://gohugo.io/) static website generator and stylized with [Bootstrap](https://getbootstrap.com/). In addition, we make use of [DataTables](https://datatables.net/) for listing anf iltering data. The code is open source (under a MIT licence) and is available on [GitLab](https://gitlab.bsc.es/inb/covid-portal/covid-portal-es/). We welcome contributions to the website - to get involved, please contact us on {{< mailto "covid19-dataportal@bsc.es" >}}.

## Organisations and people behind the portal

The Spanish COVID-19 Data Portal is operated by the [Barcelona Supercomputing Center](https://www.bsc.es/), BSC, and the [Institute of Health Carlos III](https://eng.isciii.es/eng.isciii.es/), ISCIII (for its acronym in Spanish), is responsible for the ISCIII-COVID-19 registry. The Spanish COVID-19 Data Portal is aligned and in coordination with the European initiative.

Many people from the ISCIII and BSC, and other organisations were involved in the initial development and content creation for the Spanish COVID-19 Data Portal. The portal and its content is managed by a team at the Barcelona Supercomputing Center (BSC). We are happy to answer your questions about data availability, data sharing, data management and all related issues; just send us an email at {{< mailto "covid19-dataportal@bsc.es" >}}.

### [Institute of Health Carlos III, ISCIII](https://eng.isciii.es/eng.isciii.es/)

<a class="d-block mb-3" href="https://eng.isciii.es/eng.isciii.es">
  <img style="height: 6rem;" alt="Instituto de Salud Carlos III logo" title="Instituto de Salud Carlos III logo" src="/images/logos/isciii-logo-horizontal.svg">
</a>

ISCIII is the leading Public Research Institution funding administering and performing biomedical research in Spain. In addition to its research activities, ISCIII is a national reference centre for specialized techniques servicing the Spanish National Health System and carries out teaching and training activities through the National School of Public Health. It also funds research projects and research networks on health sciences through the Fund for Research in Health Sciences (Fondo de Investigaciones Sanitarias, FIS), and houses the National Library of Health Sciences.

### [Barcelona Supercomputing Center, BSC](https://www.bsc.es/)

<a class="d-block mb-3" href="https://www.bsc.es">
  <img style="height: 6rem;" alt="Barcelona Supercomputing Center logo" title="Barcelona Supercomputing Center" src="/images/logos/bsc-logo-blue.svg">
</a>

BSC is the national supercomputing centre in Spain. BSC specialises in High Performance Computing (HPC) and its mission is two-fold: to provide infrastructure and supercomputing services to European scientists, and to generate knowledge and technology to transfer to business and society. BSC is a first level hosting member of the European research infrastructure PRACE (Partnership for Advanced Computing in Europe) and manages the Spanish Supercomputing Network (RES). BSC is a consortium formed by the Ministry of Science, Innovation and Universities of the Spanish Government, the Business and Knowledge Department of the Catalan Government and the Universitat Politecnica de Catalunya – BarcelonaTech.

### [ELIXIR Spain, ELIXIR-ES](https://inb-elixir.es/)

<a class="d-block mb-3" href="https://inb-elixir.es">
  <img style="height: 6rem;" alt="ELIXIR Spain logo" title="ELIXIR Spain" src="/images/logos/elixir-es-logo.png">
</a>

Spain has been a member of [ELIXIR](http://elixir-europe.org/) (European Life Science Infrastructure for Biological Information) since 2015. The Spanish node follows a decentralised model of nodes with a central coordination node. ELIXIR-ES is formed by 19 research groups distributed across 13 institutions in Spain, with a coordination node at the BSC. These groups traditionally functioned under the umbrella of the Spanish National Bioinformatics Institute (INB), created in 2003.

The two main overarching objectives are (i) maintaining and increasing the alignment with ELIXIR looking for deeper synergies, while (ii) increasing the translational capacity towards the Spanish National Health System (SNS).

The Spanish node serves in the coordination, integration and development of Spanish bioinformatics resources in projects in the areas of genomics, proteomics and translational medicine. It has contributed to the creation of a consistent computational infrastructure in the area of bioinformatics, participated in national and international genome projects, and trained bioinformatics users and developers.

Within ELIXIR, the Spanish node is responsible for the bioinformatics infrastructure offered by Spain, and therefore, should guarantee its alignment to the strategic areas put forward by ELIXIR. The node maintains a number of important and widely used core resources that cover fields such as functional genomics, transcriptomics, genotyping, genomic medicine, structural bioinformatics and molecular dynamics simulations. Furthermore a number of specialized tools and services are offered that complement this offering.

ELIXIR-ES has impulsed and coordinates [TransBioNet](https://inb-elixir.es/transbionet) (Translational Bioinformatics Network) with the objective of creating an expert forum where bioinformaticians working in healthcare settings can share best practices and working experiences, have early access to the latest technological developments and guidelines by international organizations and initiatives such as ELIXIR and GA4GH (Global Alliance for Genomics and Health), and benefit from access to the computational infrastructures provided by the ELIXIR and ELIXIR-ES.

</div>
