---
title: Health variables
menu:
  main:
    name: Health variables
    weight: 5
type: health-variables
tabs:
  - ids:
      - name: "Overview"
        id: "health_variables"
    title: "Hospital Universitario 12 de Octubre + Hospital Clínic de Barcelona efforts"
    content:
      - text: "The ISARIC-WHO Case Report Forms (CRFs) are used to collect data on individuals presenting with suspected or confirmed COVID-19 diagnosis. This resource has made available a collection of standardised clinical data to support patient care and COVID-19 research worldwide."
      - text: 'Hospital Universitario 12 de Octubre efforts have made possible the collection of observational variables associated to COVID-19 cases from Electronical Health Records using a flexible methodology based on Detailed Clinical Models (see the <a href="https://www.sciencedirect.com/science/article/pii/S1532046421000265?via%3Dihub">full article</a>).'
      - text: '<figure><img src="/images/diagrams/health-variables-figure.png" class="img-fluid" alt="Spanish clinical data diagram"><figcaption>This figure associates observational variables between the ISARIC COVID-19 Core Case Record Form (February 2, 2021) (<a href="https://isaric.org/research/covid-19-clinical-research-resources/covid-19-crf/">resource</a>) and the Dual Model of Observations from Hospital Universitario 12 de Octubre data models.</figcaption></figure>'
  - ids:
      - name: "Clinical variables"
        id: "clinical_variables"
    title: "Clinical variables - ISARIC based model"
    content:
      - text: "COVID-19 ranks as the single largest health incident worldwide in decades. In such a scenario, electronic health records (EHRs) should provide a timely response to healthcare needs and to data uses that go beyond direct medical care and are known as secondary uses, which includes biomedical research. However, it is usual for each data analysis initiative to define its own information model in line with its requirements. These specifications share clinical concepts, but differ in format and recording criteria, something that creates data entry redundancy in multiple electronic data capture systems (EDCs) with the consequent investment of effort and time by the organization."
      - text: "This study was undertaken at the Hospital Universitario 12 de Octubre, a 1,300-bed tertiary hospital situated in Madrid Region (Spain). It sought to design and implement a flexible methodology based on detailed clinical models (DCM), which would enable EHRs generated in a tertiary hospital to be effectively reused without loss of meaning and within a short time."
      - text: "As part of the first stage of the methodology of this study a specification of an initial and expandable set of relevant concepts for COVID-19 was identified, modeled and formalized using ISO-13606 standard and SNOMED CT and LOINC terminologies.
      - text: The standard catalog of observable entities in COVID-19 is the specification and standardization of a set of 22 clinical observable entities and 36 laboratory-related observable entities of interest in COVID-19. These concepts, in consonance with the ISO 13606 standard and semantically linked to standard terminologies, are implemented in the multiple Hospital healthcare information systems, allowing homogenous data entry via clinical record forms or, transparently, through integration with laboratory equipment. Data are stored in each system’s database, following a dual key-value structure: standard concept of the observable entities and finding reported. This allows the reuse of data, while maintaining their original meaning unaltered."
      - text: "The standard catalog and additional results of this study are the deliverables defined in the different stages of the methodology of the scientific publication cited below. Its implementation into the Hospital 12 de Octubre began on March 15, 2020 and the first EHR-derived extract was generated and validated on April 20, 2020."
      - text: 'Pedrera Jiménez, Miguel <span class="font-italic">et al.</span> <strong>Obtaining EHR-derived datasets for COVID-19 research within a short time: a flexible methodology based on Detailed Clinical Models.</strong> <span class="font-italic">Journal of biomedical informatics, 103697</span>. Feb 3 2021. DOI: <a target="_blank" href="https://doi.org/10.1016/j.jbi.2021.103697">10.1016/j.jbi.2021.103697</a>'
        header: "Publication"
    data: "/files/health_isaric_variables.csv"
    partial: "health_variables/12o-model-isaric-table.html"
  - ids:
      - name: "Chronic diseases variables"
        id: "chronic_diseases_variables"
    title: "Chronic diseases variables - cohorts based"
    content:
      - text: 'In the framework of the <a target="_blank" href="https://www.isglobal.org/en/-/covicat">COVICAT study</a>, the authors defined the chronic disease categories using the available information from COVICAT survey. The table contains self‐reported variables about chronic diseases within the COVICAT cross‐sectional survey that was answered by the participants between May 28th and July 31st 2020. Chronic diseases were defined as any long‐term condition lasting 6 or more months, being considered the variables H13_A2 to H13_A17. Those referring to asthma, chronic bronchitis and COPD, allergies and related treatments are considered in the variables H02 to H08.'
      - text: "This information has been extracted from a document prepared by Magda Bosch de Basea & Judith Garcia‐Aymerich in December, 2020."
      - text: 'The project aims to evaluate the evolution of incidences of infection by the coronavirus SARS-CoV-2 in the Catalan population during 12 months through a prospective epidemiological study of populational cohorts established before the pandemic. The study is being carried out with 24,000 people who volunteered and who are already members of different populational cohorts in a collaboration between the GCAT Genomes for Life Project of the Germans Trias i Pujol Research Institute (IGTP) and two research groups from ISGlobal, a centre supported by the "la Caixa" Foundation, and the Hospital Clínic, Barcelona.'
        header: "About COVICAT project"
    data: "/files/health_covicat_variables.csv"
    partial: "health_variables/covicat-table.html"
  - ids:
      - name: "Specific projects variables"
        id: "specific_projects_variables"
    title: "COVID-19 ISCIII's funded projects variables"
    content:
      - text: "The table below shows health variables reported by projects funded by the ISCIII funds Fondo COVID19. This information will be updated regularly as projects report new variables."
    data: "/files/health_isciii_variables.csv"
    partial: "health_variables/isciii-table.html"
---
