---
title: Submit data
---

<div class="container">

## Submit data

The COVID-19 Data Portal draws data from a number of databases. In order to share new COVID-19 data, please select from the following resources:

- [Viral, non-human and cell line sequence data](#viral-non-human-line-sequence)
- [Human molecular biology data](#human-molecular-biology-data)
- [Linked viral and human molecular biology data](#linked-viral-human-molecular-biology-data)
- [Viral and non-human proteomics data](#viral-non-human-proteomics-data)
- [Structural biology data](#structural-biology-data)
- [Viral and non-human molecular interaction data](#viral-non-human-molecular-interaction-data)
- [Viral and non-human metabolomics data](#viral-non-human-metabolomics-data)
- [Viral and other non-human molecular biology data](#viral-non-human-molecural-biology-data)
- [Compound and target data](#compound-target-data)
- [Clinical and epidemiological data](#clinical-epidemiological-data)
- [Non-biological data](#non-biological-data)

Find a more general list of resources in the [ELIXIR recommended Deposition Databases](https://elixir-europe.org/platforms/data/elixir-deposition-databases) for Biomolecular Data.

### <a name="viral-non-human-line-sequence"></a>Viral, non-human and cell line sequence data

This class includes sequence data from studies targeting virus alone or with co-occurring species. It also includes sequencing from non-human host species (such as from species acting as models for infection) and human cell lines (where data are consented for full open publication). All sequencing library types, all platforms, all library methods and all levels of processing (from raw data to assembled sequences) are included in this class.

Deposition actions:

- [Users should submit data to ENA](https://www.ebi.ac.uk/ena/browser/home)
- [Specific deposition instructions are available for viral data submission](https://ena-browser-docs.readthedocs.io/en/latest/help_and_guides/sars-cov-2-submissions.html)
- [Guide to submit SARS-CoV-2 sequence data to ENA developed by ELIXIR-BE](https://rdm.elixir-belgium.org/covid-19/sarscov2_submission.html)
- Users are encouraged to contact ENA at <virus-dataflow@ebi.ac.uk>

General depositions and those from users who are managing their data in SARS-CoV-2 Data Hubs are also included in this class.

### <a name="human-molecular-biology-data">Human molecular biology data

This class covers all sequence and all other molecular biology data types of human source where the data are potentially identifying of the research subject and must therefore be managed under controlled access.

Deposition actions:

- [Users should submit data to European Genome-phenome Archive (EGA)](https://ega-archive.org/)
- Users should contact their national nodes of EGA or other national infrastructure for the management of these data

### <a name="linked-viral-human-molecular-biology-data">Linked viral and human molecular biology data

This case covers data from studies that consider in combination SARS-CoV-2 and the human research subject that is infected with it. Studies include combined sequencing of host transcriptome and viral genotype; immunome and viral gene expression; pulmonary proteomics and targeted sequence-based typing of viral genes.

Deposition actions:

- [Users should register data first into the BioSamples Database](https://www.ebi.ac.uk/biosamples/)
- Users are encouraged to contact EMBL-EBI at <virus-dataflow@ebi.ac.uk>

### <a name="viral-non-human-proteomics-data">Viral and non-human proteomics data

Data in this class are derived from spectroscopy-based analyses of occurrence and abundance profiles for proteins in non-human samples, including in vitro viral culture systems.

Deposition actions:

- [Users should deposit data into the PRoteomics IDentification Database (PRIDE)](https://www.ebi.ac.uk/pride/archive/)

### <a name="structural-biology-data">Structural biology data

This class covers data from methods that elucidate the 3-dimensional structure of proteins and other biological macromolecules in isolation, or in their biological context (such as with bound ligands).

Deposition actions:

- [Users should deposit x-ray crystallography and nuclear magnetic resonance data to the Protein Data Bank in Europe (PDBE)](https://www.ebi.ac.uk/pdbe/)
- [Users should deposit electron microscopy and tomography data at the Electron Microscopy Public Image Archive (EMPIAR)](https://www.ebi.ac.uk/pdbe/emdb/empiar/)

### <a name="viral-non-human-molecular-interaction-data">Viral and non-human molecular interaction data

In this class are data from studies that seek to elucidate the interactions between macromolecules at binary, complex and network scales.

Deposition actions:

- [Users should deposit molecular interactions data to IntAct.](https://www.ebi.ac.uk/intact/)

### <a name="viral-non-human-metabolomics-data">Viral and non-human metabolomics data

This class comprises spectroscopy-based profiling of metabolites in non-human tissues, such as virus-infected model species and cell lines.

Deposition actions:

- [Users should deposit metabolomics data to MetaboLights.](http://www.ebi.ac.uk/metabolights/presubmit)

### <a name="viral-non-human-molecular-biology-data">Viral and other non-human molecular biology data

Non-human molecular biological data of relevance to COVID-19 research for which a structured data deposition database does not exist lie in this class, such as qPCR or viral virulence assay data.

Deposition actions:

- [Users should deposit these data to BioStudies.](https://www.ebi.ac.uk/biostudies/)

### <a name="compound-target-data">Compound and target data

Studies with focus on compounds and the investigation of their biological activities with respect to viral and other drug targets lie in this class.

Deposition actions:

- [Users should deposit these data to ChEMBL](https://www.ebi.ac.uk/chembl/)

### <a name="clinical-epidemiological-data">Clinical and epidemiological data

Data in this class cover research subject data captured during clinical practice or study relating to cohorts, study groups and case studies and include such data types as serology profiles, treatment histories and disease classifications. These data are typically retained within national health data systems and not shared centrally, but are ideally linked to other centrally managed data.

Deposition actions:

- Users should provide top-level cohort and study group data to <virus-dataflow@ebi.ac.uk>
- Data should be provided to national health data systems and, where possible, linked to/from data accessible from the COVID-19 Portal
- [Where possible, data can be managed within EGA](https://ega-archive.org/)

### <a name="non-biological-data">Non-biological data data

Non-biological data of relevance to COVID-19, such as travel, trade, meteorology and social distancing behaviour are not managed within the European COVID-19 Data Platform, but where possible are linked to data within the system.

Deposition actions:

- Users should encourage data standards compliance in these non-biological data sets, such as relates to the World Geospatial Consortium and FAIR

</div>
