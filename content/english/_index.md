---
title: Home
---

The **Spanish COVID-19 Data Portal** provides information, guidelines, tools and services to support researchers in utilising Spanish and European infrastructures for data sharing, in particular the [European COVID-19 Data Portal](https://covid19dataportal.org). For those interested in ongoing large research projects in Spain, we have compiled a [list of projects funded by major funding agencies](/research-projects).
