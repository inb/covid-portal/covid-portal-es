var selected_area_id = null;
var province_id_to_name = {'48': 'Bizkaia', '13': 'Ciudad Real', '26': 'La Rioja', '22': 'Huesca', '20': 'Gipuzkoa', '46': 'València/Valencia', '15': 'A Coruña', '18': 'Granada', '31': 'Navarra', '33': 'Asturias', '34': 'Palencia', '51': 'Ceuta', '29': 'Málaga', '06': 'Badajoz', '07': 'Illes Balears', '49': 'Zamora', '45': 'Toledo', '16': 'Cuenca', '17': 'Girona', '10': 'Cáceres', '11': 'Cádiz', '09': 'Burgos', '42': 'Soria', '43': 'Tarragona', '21': 'Huelva', '28': 'Madrid', '08': 'Barcelona', '52': 'Melilla', '03': 'Alacant/Alicante', '12': 'Castelló/Castellón', '23': 'Jaén', '27': 'Lugo', '05': 'Ávila', '47': 'Valladolid', '04': 'Almería', '25': 'Lleida', '02': 'Albacete', '32': 'Ourense', '36': 'Pontevedra', '39': 'Cantabria', '44': 'Teruel', '50': 'Zaragoza', '35': 'Las Palmas', '19': 'Guadalajara', '40': 'Segovia', '24': 'León', '14': 'Córdoba', '01': 'Araba/Álava', '30': 'Murcia', '37': 'Salamanca', '38': 'Santa Cruz de Tenerife', '41': 'Sevilla'}
var apiURL = 'https://flowmaps.life.bsc.es/api'
var colorOutgoing = '#240047'
var colorOutgoingArrows = '#240047'
var colorIncoming = '#950000'
var colorIncomingArrows = '#300b0b' // #575757 #330404
var colorIncidence = '#000059' // #050559 #03032e #030333 #000059
var colorBase = '#e9e9e9'
var plotly_options = {displaylogo: false, displayModeBar: false, modeBarButtonsToRemove: ['toggleHover', 'hoverClosestGeo', 'pan2d', 'select2d', 'lasso2d']}

var date_str = null;
function set_risk_dates() {
    // Set the date_str to the max date of flowmaps has data from the sources
    return fetch(apiURL + '/risk_dates')
    .then(response => response.json())
    .then(function(data) {
        date_str = data['max_date']
    });
}


async function createRiskMaps(div) {
    // Get the data from the API REST, and then, draw the maps. It's needed because the scale of the colorbar
    // has to be common.

    await set_risk_dates()

    var incoming_risk_filters = {
        "date": date_str,
        "source_layer": 'cnig_provincias',
        "target_layer": 'cnig_provincias',
        "ev": 'ES.covid_cpro',
    }
    var outgoing_risk_filters = {
        "date": date_str,
        "source_layer": 'cnig_provincias',
        "target_layer": 'cnig_provincias',
        "ev": 'ES.covid_cpro',
    }


    Promise.all([
        fetch('cnig_provincias.geojson'),
        fetch(apiURL+'/aggregated_incoming_risk/?where='+JSON.stringify(incoming_risk_filters)),
        fetch(apiURL+'/aggregated_outgoing_risk/?where='+JSON.stringify(outgoing_risk_filters))
    ]).then(function (responses) {
        return Promise.all(responses.map(function (response) {
            return response.json()
        }));
    }).then(function (data) {
        var geojson = data[0];
        var incoming_risk = data[1]['_items'];
        var outgoing_risk = data[2]['_items'];

        var incoming_zmax = Math.max.apply(null, incoming_risk.map(x=>x.incoming_risk));
        var outgoing_zmax = Math.max.apply(null, outgoing_risk.map(x=>x.outgoing_risk));
        var zmax = Math.max(incoming_zmax, outgoing_zmax);

        $('#' + div)[0].innerHTML =
            `<div id='incoming_outgoing_risk' class="row">` +
                `<div class="col-lg-6 col-md-12" style="padding: 2px">` +
                    `<h5>Incoming risk</h5>` +
                    `<div class="chart" id="incomingRiskMap" style="border: 1px solid grey; overflow-x: hidden;"></div>` +
                    `<div class="hoverinfo-bottom-right" id="hoverinfo_incoming"></div>` +
                `</div>` +
                `<div class="col-lg-6 col-md-12" style="padding: 2px">` +
                    `<h5>Outgoing risk</h5>` +
                    `<div class="chart" id="outgoingRiskMap" style="border: 1px solid grey; overflow-x: hidden;"></div>` +
                    `<div class="hoverinfo-bottom-right" id="hoverinfo_outgoing"></div>` +
                `</div>` +
            `</div>`
        createIncomingRiskMap(geojson, incoming_risk, zmax);
        createOutgoingRiskMap(geojson, outgoing_risk, zmax);
    });
}


function selectAreaRiskMaps(area_id) {
    var incoming_risk_filters = {
        "target": area_id,
        "date": date_str,
        "source_layer": 'cnig_provincias',
        "target_layer": 'cnig_provincias',
        "ev": 'ES.covid_cpro',
    }
    var outgoing_risk_filters = {
        "source": area_id,
        "date": date_str,
        "source_layer": 'cnig_provincias',
        "target_layer": 'cnig_provincias',
        "ev": 'ES.covid_cpro',
    }

    Promise.all([
        fetch(apiURL+'/incoming_risk/?where='+JSON.stringify(incoming_risk_filters)),
        fetch(apiURL+'/outgoing_risk/?where='+JSON.stringify(outgoing_risk_filters)),
    ]).then(function (responses) {
        return Promise.all(responses.map(function (response) {
            return response.json()
        }));
    }).then(function (data) {
        var incoming_risk = data[0]._items
        var outgoing_risk = data[1]._items

        var incoming_zmax = Math.max.apply(null, incoming_risk.map(x=>x.incoming_risk));
        var outgoing_zmax = Math.max.apply(null, outgoing_risk.map(x=>x.outgoing_risk));
        var zmax = Math.max(incoming_zmax, outgoing_zmax);

        selectAreaIncomingRiskMap(area_id, incoming_risk, zmax);
        selectAreaOutgoingRiskMap(area_id, outgoing_risk, zmax);
    });
}


function selectAreaIncomingRiskMap(area_id, risk, zmax) {
    risk = risk.filter(a=>a.incoming_risk > 0.01)
    risk = risk.sort((a, b)=>a.incoming_risk < b.incoming_risk ? 1: -1)

    var graphDiv = document.getElementById('incomingRiskMap')
    var features = graphDiv.data[0].geojson.features
    var selectedFeature = features.find(y=>y.id == area_id)

    // update local risk trace
    graphDiv.data[0].locations = risk.map(x=>x.source)
    graphDiv.data[0].customdata = risk.map(x=>[province_id_to_name[x.source], x.trips.toFixed(0), x.incoming_risk.toFixed(2), x.source_active_cases_7]);
    graphDiv.data[0].z = risk.map(x=>x.incoming_risk)
    graphDiv.data[0].zmax = zmax
    // graphDiv.data[0].hovertemplate = "%{customdata[0]}<br>Risk for "+province_id_to_name[area_id]+": %{customdata[2]}<br>Trips going to "+province_id_to_name[area_id]+": %{customdata[1]}<br>Covid-19 cases (last week): %{customdata[3]}<extra></extra>";
    graphDiv.data[0].hovertemplate = "%{customdata[0]}<extra></extra>";
    graphDiv.data[0].colorscale = [['0.0', colorBase], ['1.0', colorIncoming]]
    graphDiv.data[0].colorbar.title.text = "Risk"

    // add polygon for the selected area
    graphDiv.data[1] = {
        type: "choroplethmapbox",
        locations: [area_id],
        geojson: {
            "type": "FeatureCollection",
            "features": features.filter(area => area.id == area_id)
        },
        z: [0],
        customdata: [[province_id_to_name[area_id], area_id]],
        hovertemplate: "%{customdata[0]}<extra></extra>",
        marker: {
            opacity: 0,
        },
        showscale: false,
        hoverlabel: {align: "left"},
    }

    graphDiv.layout.mapbox.layers = [
        {
            "color": "black",
            "line": {"width": 2.5},
            "opacity": 1,
            "sourcetype": "geojson",
            "type": "line",
            "source": {
                "type": "FeatureCollection",
                "features": [selectedFeature]
            }
        },
        {
            "sourcetype": "geojson",
            "type": "fill",
            "opacity": 0.5,
            "color": "yellow",
            "source": {
                "type": "FeatureCollection",
                "features": [selectedFeature]
            }
        }
    ];

    // Build arrows
    var topRiskIds = risk.slice(0, 10).map(x=>x.source)  // top 10 local areas
    graphDiv.layout.mapbox.layers.push(
        createArrowsGeojson(features, selectedFeature.centroid, topRiskIds, incoming=true))

    Plotly.redraw('incomingRiskMap')
}


function createIncomingRiskMap(geojson, risk, zmax) {
    var data = [
        {
            type: "choroplethmapbox",
            locations: risk.map(x=>x.id),
            z: risk.map(x=>x.incoming_risk),
            customdata: risk.map(x=>[province_id_to_name[x.id], x.incoming_risk]),
            geojson: geojson,
            // hovertemplate: "%{customdata[0]}<br>Total incoming risk: %{z}<extra></extra>",
            hovertemplate: "%{customdata[0]}<extra></extra>",
            marker: {
                opacity: 0.6,
            },
            colorbar: {title: {text: "Total<br>incoming<br>risk"}, thickness: 10, x: 0},
            // colorscale: 'Reds',
            colorscale: [['0.0', colorBase], ['1.0', colorIncoming]],
            hoverlabel: {align: "left"},
            zmin: 0,
            zmax: zmax,
        }
    ];

    var layout = {
        mapbox: {
            center: {
                lon: -3.717336960173357, lat: 40.495178477814555
                // lon: -8, lat: 36 // shift center to show Canarias
            },
            zoom: 3.7,
            style: 'carto-positron',
        },
        height: 300,
        margin: {"r":0,"t":0,"l":0,"b":0},
        modebar: {'bgcolor': 'transparent', 'color': '#b7baba'},
    };

    Plotly.plot('incomingRiskMap', data, layout, plotly_options).then(gd => {
        gd.on('plotly_click', (e) => {
            if (selected_area_id != e.points[0].location) {
                // DIFFERENT AREA THAN THE PREVIOUSLY SELECTED
                selected_area_id = e.points[0].location
                selectAreaRiskMaps(selected_area_id);
            } else {
                // SAME AREA THAN THE PREVIOUSLY SELECTED
                resetMaps()
                selected_area_id = null
            }
        });

        gd.on('plotly_hover', (e) => {
            $('#hoverinfo_outgoing')[0].style.visibility = 'hidden';

            var customdata = e.points[0].customdata;
            if (selected_area_id == null) {

                $('#hoverinfo_incoming')[0].innerHTML = customdata[0]+"<br>Total incoming risk: "+customdata[1].toFixed(2);
                $('#hoverinfo_incoming')[0].style.visibility = 'visible';

            } else {

                if (e.points[0].location != selected_area_id) {
                    $('#hoverinfo_incoming')[0].innerHTML = customdata[0]+
                        "<br>Risk for "+province_id_to_name[selected_area_id]+": "+customdata[2]+
                        "<br>Trips going to "+province_id_to_name[selected_area_id]+": "+customdata[1]+
                        "<br>Covid-19 cases (last week): "+customdata[3]
                    $('#hoverinfo_incoming')[0].style.visibility = 'visible';
                } else {
                    $('#hoverinfo_incoming')[0].innerHTML = customdata[0]
                    $('#hoverinfo_incoming')[0].style.visibility = 'visible';
                }
            }
        });

        gd.on('plotly_unhover', (e) => {
            $('#hoverinfo_incoming')[0].style.visibility = 'hidden';
            $('#hoverinfo_outgoing')[0].style.visibility = 'hidden';
        });

        gd.on('plotly_relayout', (e) => {
            // update zoom in the other map
            var graphDiv = document.getElementById('outgoingRiskMap')
            var update = {
                'mapbox.zoom': e['mapbox.zoom'],
                'mapbox.center': e['mapbox.center']
            }
            Plotly.update('outgoingRiskMap', {}, update)
        });

        gd.on('plotly_relayouting', (e) => {
            // update just pan in the other map
            var graphDiv = document.getElementById('outgoingRiskMap')
            if (graphDiv.layout.mapbox.zoom != e['mapbox.zoom']) {
                return;
            }
            var update = {
                // 'mapbox.zoom': e['mapbox.zoom'],
                'mapbox.center': e['mapbox.center']
            }
            Plotly.update('outgoingRiskMap', {}, update)
        });

    });
}


function selectAreaOutgoingRiskMap(area_id, risk, zmax) {
    risk = risk.filter(a=>a.outgoing_risk > 0.01)
    risk = risk.sort((a, b)=>a.outgoing_risk < b.outgoing_risk ? 1: -1)

    var graphDiv = document.getElementById('outgoingRiskMap')
    var features = graphDiv.data[0].geojson.features
    var selectedFeature = features.find(y=>y.id == area_id)

    // update local risk trace
    graphDiv.data[0].locations = risk.map(x=>x.target)
    graphDiv.data[0].customdata = risk.map(x=>[province_id_to_name[x.target], x.trips.toFixed(0), x.outgoing_risk.toFixed(2)]);
    graphDiv.data[0].z = risk.map(x=>x.outgoing_risk)
    graphDiv.data[0].zmax = zmax
    // graphDiv.data[0].hovertemplate = "%{customdata[0]}<br>Risk from "+province_id_to_name[area_id]+": %{customdata[2]}<br>Trips coming from "+province_id_to_name[area_id]+": %{customdata[1]}<extra></extra>";
    graphDiv.data[0].hovertemplate = "%{customdata[0]}<extra></extra>";
    graphDiv.data[0].colorscale = [['0.0', colorBase], ['1.0', colorOutgoing]]
    graphDiv.data[0].colorbar.title.text = "Risk"

    // add polygon for the selected area
    graphDiv.data[1] = {
        type: "choroplethmapbox",
        locations: [area_id],
        geojson: {
            "type": "FeatureCollection",
            "features": features.filter(area => area.id == area_id)
        },
        z: [0],
        customdata: [[province_id_to_name[area_id], area_id, risk[0].source_active_cases_7]],
        // hovertemplate: "%{customdata[0]}<br>Covid-19 cases (last week): %{customdata[2]}<extra></extra>",
        hovertemplate: "%{customdata[0]}<extra></extra>",
        marker: {
            opacity: 0,
        },
        showscale: false,
        hoverlabel: {align: "left"},
    }

    graphDiv.layout.mapbox.layers = [
        {
            "color": "black",
            "line": {"width": 2.5},
            "opacity": 1,
            "sourcetype": "geojson",
            "type": "line",
            "source": {
                "type": "FeatureCollection",
                "features": [selectedFeature]
            }
        },
        {
            "sourcetype": "geojson",
            "type": "fill",
            "opacity": 0.5,
            "color": "yellow",
            "source": {
                "type": "FeatureCollection",
                "features": [selectedFeature]
            }
        }
    ];

    // Build arrows
    var topRiskIds = risk.slice(0, 10).map(x=>x.target)  // top 10
    graphDiv.layout.mapbox.layers.push(
        createArrowsGeojson(features, selectedFeature.centroid, topRiskIds, incoming=false))

    Plotly.redraw('outgoingRiskMap')
}


function createOutgoingRiskMap(geojson, risk, zmax) {
    var data = [
        {
            type: "choroplethmapbox",
            locations: risk.map(x=>x.id),
            z: risk.map(x=>x.outgoing_risk),
            customdata: risk.map(x=>[province_id_to_name[x.id], x.outgoing_risk]),
            geojson: geojson,
            // hovertemplate: "%{customdata[0]}<br>Total outgoing risk: %{z}<extra></extra>",
            hovertemplate: "%{customdata[0]}<extra></extra>",
            marker: {
                opacity: 0.6,
            },
            // colorbar: {title: {text: "Total<br>outgoing<br>risk"}, thickness: 10, xanchor: 'right', x: 1},
            colorbar: {title: {text: "Total<br>outgoing<br>risk"}, thickness: 10, x: 0},
            // colorscale: 'Reds',
            colorscale: [['0.0', colorBase], ['1.0', colorOutgoing]],
            hoverlabel: {align: "left"},
            zmin: 0,
            zmax: zmax,
        }
    ];

    var layout = {
        mapbox: {
            center: {
                lon: -3.717336960173357, lat: 40.495178477814555
                // lon: -8, lat: 36 // shift center to show Canarias
            },
            zoom: 3.7,
            style: 'carto-positron',
        },
        height: 300,
        margin: {"r":0,"t":0,"l":0,"b":0},
        modebar: {'bgcolor': 'transparent', 'color': '#b7baba'},
    };

    Plotly.plot('outgoingRiskMap', data, layout, plotly_options).then(gd => {
        gd.on('plotly_click', (e) => {
            if (selected_area_id != e.points[0].location) {
                // DIFFERENT AREA THAN THE PREVIOUSLY SELECTED
                selected_area_id = e.points[0].location
                selectAreaRiskMaps(selected_area_id);
            } else {
                // SAME AREA THAN THE PREVIOUSLY SELECTED
                resetMaps()
                selected_area_id = null
            }
        });
        gd.on('plotly_hover', (e) => {
            $('#hoverinfo_incoming')[0].style.visibility = 'hidden';

            var customdata = e.points[0].customdata;
            if (selected_area_id == null) {

                $('#hoverinfo_outgoing')[0].innerHTML = customdata[0]+"<br>Total outgoing risk: "+customdata[1].toFixed(2);
                $('#hoverinfo_outgoing')[0].style.visibility = 'visible';

            } else {

                if (e.points[0].location != selected_area_id) {
                    $('#hoverinfo_outgoing')[0].innerHTML = customdata[0]+
                        "<br>Risk from "+province_id_to_name[selected_area_id]+": "+customdata[2]+
                        "<br>Trips coming from "+province_id_to_name[selected_area_id]+": "+customdata[1]
                    $('#hoverinfo_outgoing')[0].style.visibility = 'visible';
                } else {
                    $('#hoverinfo_outgoing')[0].innerHTML = customdata[0]+"<br>Covid-19 cases (last week): "+customdata[2]
                    $('#hoverinfo_outgoing')[0].style.visibility = 'visible';
                }
            }
        });

        gd.on('plotly_unhover', (e) => {
            $('#hoverinfo_outgoing')[0].style.visibility = 'hidden';
            $('#hoverinfo_incoming')[0].style.visibility = 'hidden';
        });


        gd.on('plotly_relayout', (e) => {
            // update zoom in the other map
            var graphDiv = document.getElementById('incomingRiskMap')
            var update = {
                'mapbox.zoom': e['mapbox.zoom'],
                'mapbox.center': e['mapbox.center']
            }
            Plotly.update('incomingRiskMap', {}, update)
        });

        gd.on('plotly_relayouting', (e) => {
            // update just pan in the other map
            var graphDiv = document.getElementById('incomingRiskMap')
            if (graphDiv.layout.mapbox.zoom != e['mapbox.zoom']) {
                return;
            }
            var update = {
                // 'mapbox.zoom': e['mapbox.zoom'],
                'mapbox.center': e['mapbox.center']
            }
            Plotly.update('incomingRiskMap', {}, update)
        });

    });
}


function resetIncomingRiskMap(risk, zmax) {
    var graphDiv = document.getElementById('incomingRiskMap')
    var features = graphDiv.data[0].geojson.features
    graphDiv.data[0].locations = risk.map(x=>x.id)
    graphDiv.data[0].customdata = risk.map(x=>[province_id_to_name[x.id], x.incoming_risk])
    graphDiv.data[0].z = risk.map(x=>x.incoming_risk)
    graphDiv.data[0].zmax = zmax
    // graphDiv.data[0].hovertemplate = "%{customdata[0]}<br>Total incoming risk: %{z}<extra></extra>"
    graphDiv.data[0].hovertemplate = "%{customdata[0]}<extra></extra>"
    graphDiv.data[0].colorbar.title.text = "Total<br>incoming<br>risk"
    graphDiv.data[0].colorscale = [['0.0', colorBase], ['1.0', colorIncoming]]
    graphDiv.layout.mapbox.layers = [] // clear the arrows and the selected polygon
    graphDiv.data.splice(1, 1)
    Plotly.redraw('incomingRiskMap')
}


function resetOutgoingRiskMap(risk, zmax) {
    var graphDiv = document.getElementById('outgoingRiskMap')
    var features = graphDiv.data[0].geojson.features
    graphDiv.data[0].locations = risk.map(x=>x.id)
    graphDiv.data[0].customdata = risk.map(x=>[province_id_to_name[x.id], x.outgoing_risk])
    graphDiv.data[0].z = risk.map(x=>x.outgoing_risk)
    graphDiv.data[0].zmax = zmax
    // graphDiv.data[0].hovertemplate = "%{customdata[0]}<br>Total outgoing risk: %{z}<extra></extra>"
    graphDiv.data[0].hovertemplate = "%{customdata[0]}<extra></extra>"
    graphDiv.data[0].colorbar.title.text = "Total<br>outgoing<br>risk"
    graphDiv.data[0].colorscale = [['0.0', colorBase], ['1.0', colorOutgoing]]
    graphDiv.layout.mapbox.layers = [] // clear the arrows and the selected polygon
    graphDiv.data.splice(1, 1)
    Plotly.redraw('outgoingRiskMap');
}


function resetMaps() {
    var filters = {
        "date": date_str,
        "source_layer": 'cnig_provincias',
        "target_layer": 'cnig_provincias',
        "ev": 'ES.covid_cpro',
    }
    Promise.all([
        fetch(apiURL+'/aggregated_incoming_risk/?where='+JSON.stringify(filters)),
        fetch(apiURL+'/aggregated_outgoing_risk/?where='+JSON.stringify(filters))
    ]).then(function (responses) {
        return Promise.all(responses.map(function (response) {
            return response.json()
        }));
    }).then(function (data) {

        var incoming_risk = data[0]['_items']
        var outgoing_risk = data[1]['_items']

        var incoming_zmax = Math.max.apply(null, incoming_risk.map(x=>x.incoming_risk));
        var outgoing_zmax = Math.max.apply(null, outgoing_risk.map(x=>x.outgoing_risk));
        var zmax = Math.max(incoming_zmax, outgoing_zmax);

        resetIncomingRiskMap(incoming_risk, zmax);
        resetOutgoingRiskMap(outgoing_risk, zmax);
    })
}


// =====================================  ARROWS  =====================================
function round(num, decimals=0){
    var k = Math.pow(10, decimals)
    return Math.round((num + Number.EPSILON) * k) / k
}

function rotate(a, theta) {
    return [a[0]*Math.cos(theta) - a[1]*Math.sin(theta), a[0]*Math.sin(theta) + a[1]*Math.cos(theta)];
}

function module(a, b) {
    var v = [b[0]-a[0], b[1]-a[1]]                            // get direction vector
    return Math.sqrt(Math.pow(v[0], 2) + Math.pow(v[1], 2))  // module
}

function createArrow(a, b, dist=0.2, markerSize=0.06) {
    // dist: how far place the arrow end from the destination
    // markerSize: arrow marker size
    var angle = Math.atan2(a[1]-b[1], a[0]-b[0]);
    var v = [b[0]-a[0], b[1]-a[1]]                    // get direction vector
    var m = Math.sqrt(Math.pow(v[0], 2) + Math.pow(v[1], 2))  // module
    var u = [v[0]/m, v[1]/m]                          // get unitary vector in the direction
    var newb = [b[0]-u[0]*dist, b[1]-u[1]*dist]       // place the arrow end point a bit before the real end
    var s1 = rotate([markerSize, markerSize], angle)  // "sides" of the arrow. Applied to a base vector in left direction <--, and rotated to match the correct angle
    var s2 = rotate([markerSize, -markerSize], angle)
    return [a, newb, [newb[0]+s1[0], newb[1]+s1[1]], newb, [newb[0]+s2[0], newb[1]+s2[1]]]
}


function createOutgoingArrow(a, b, dist=0.1, markerSize=0.06) {
    // dist: how far place the arrow end from the destination
    // markerSize: arrow marker size
    var angle = Math.atan2(a[1]-b[1], a[0]-b[0]);
    var v = [b[0]-a[0], b[1]-a[1]]                    // get direction vector
    var m = Math.sqrt(Math.pow(v[0], 2) + Math.pow(v[1], 2))  // module
    var u = [v[0]/m, v[1]/m]                          // get unitary vector in the direction
    var newa = [a[0]+u[0]*dist, a[1]+u[1]*dist]       // place the arrow start point a bit before the real start
    var s1 = rotate([markerSize, markerSize], angle)  // "sides" of the arrow. Applied to a base vector in left direction <--, and rotated to match the correct angle
    var s2 = rotate([markerSize, -markerSize], angle)
    return [newa, b, [b[0]+s1[0], b[1]+s1[1]], b, [b[0]+s2[0], b[1]+s2[1]]]
}


function createArrowsGeojson(features, center, arrowsIds, incoming=true) {
    var lines = features.filter(x=>arrowsIds.includes(x.id)).map(x => incoming ? [x.centroid, center] : [center, x.centroid]);
    var arrows = lines.map(x => createArrow(x[0], x[1]));
    if (incoming) {
        var color = colorIncomingArrows;
        var arrows = lines.map(x => createArrow(x[0], x[1]));
    } else {
        var color = colorOutgoingArrows;
        var arrows = lines.map(x => createOutgoingArrow(x[0], x[1]));
    }
    // var arrows = lines.map(x => x.concat(createArrow(x[0], x[1])));
    // var arrows = lines.map(x => x);
    return {
        'sourcetype': 'geojson',
        'source': {
            "type": "MultiLineString",
            "coordinates": arrows
        },
        'below': '', 'type': 'line',
        'color': color, 'opacity': 1,
        'line': {'width': 2}
    }
}