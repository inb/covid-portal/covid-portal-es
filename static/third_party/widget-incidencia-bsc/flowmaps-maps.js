var selected_province = null;
var apiURL = 'https://flowmaps.life.bsc.es/api'
var colorIncidence = '#000059' // #050559 #03032e #030333 #000059
var colorBase = '#e9e9e9'
var plotly_options = {
    displaylogo: false,
    displayModeBar: false,
    modeBarButtonsToRemove: ['toggleHover', 'hoverClosestGeo', 'pan2d', 'select2d', 'lasso2d'],
    responsive: true,
}

var date_str = null;
function set_risk_dates() {
    // Set the date_str to the max date of flowmaps has data from the sources
    return fetch(apiURL + '/incidence_dates')
    .then(response => response.json())
    .then(function(data) {
        date_str = data['max_date']
    });
}


async function createRiskMaps(div, height) {
    // Get the data from the API REST, and then, draw the maps. It's needed because the scale of the colorbar
    // has to be common.

    await set_risk_dates()

    let filters = {
        "ev": "ES.covid_cpro",
        "start_date": date_str,
        "end_date": date_str,
    }
    let filters2 = {
        "type": "population",
        "date": '2020-03-01',
        "layer": "cnig_provincias",
    }

	Promise.all([
		fetch('cnig_provincias.geojson'),
        fetch(apiURL+'/incidence/?where='+JSON.stringify(filters)),
		fetch(apiURL+'/layers.data.consolidated/?where='+JSON.stringify(filters2)+'&max_results=500'),
	]).then(function (responses) {
		return Promise.all(responses.map(function (response) {
			return response.json();
		}));
	}).then(function (data) {
		var provinciasGeojson = data[0];

		// covidData: {1: {"active_cases_14": 3, ...}, 2: {...}, 3: {...}}
        var covidData = {};
        data[1]._items[0].data.forEach(d => {
            covidData[d.id] = d;
        });

        // popData: {1: 300, 2: 450, 3: 403, ...}
        var popData = {};
        data[2]._items.forEach(d => {
            popData[d.id] = d;
        })

        var provincesNames = {};
        provinciasGeojson.features.forEach(feat => provincesNames[feat.id] = feat.name);
        var locations = Object.keys(covidData);

		var layout = {
			mapbox: {
				center: {
					lon: -4, lat: 40.095178477814555
				},
				zoom: 4.5,
				style: 'carto-positron',
			},
			height: height,
			margin: {"r":0,"t":0,"l":0,"b":0},
		};
        var data = [create_data_for_map(locations, covidData, popData, provincesNames, provinciasGeojson)];

	    Plotly.newPlot(div, data, layout, plotly_options).then(gd => {
			gd.on('plotly_click', (e) => {
				selected_province = e.points[0].location;

				// Border of the province selected bigger
                $('#' + div)[0].layout.mapbox.layers = [
                    {
                        "color": "black",
                        "line": {"width": 2.5},
                        "opacity": 1,
                        "sourcetype": "geojson",
                        "type": "line",
                        "source": {
                            "type": "FeatureCollection",
                            "features": [provinciasGeojson.features.find(y=>y.id == selected_province)]
                        }
                    }
                ];
                Plotly.redraw(div)

				// Open abs tab
				has_to_recreate_local_map = true;
				$("#abs-tab").click();
			});

            gd.on('plotly_hover', (e) => {
                var customdata = e.points[0].customdata
                var z = e.points[0].z
                var text = `Date: ${date_str}<br>` +
                    `Province: ${customdata[0]}<br>` +
                    `Population: ${customdata[1]}<br>` +
                    `Daily incidence:<br>` +
                    ` - Daily incidence: ${customdata[2]}<br>` +
                    ` - Daily incidence per 100k population: ${customdata[3]}<br>` +
                    `Accumulated incidence:<br>` +
                    ` - Incidence (7 days): ${customdata[4]}<br>` +
                    ` - Incidence (14 days): ${customdata[5]}<br>` +
                    ` - Incidence (14 days) per 100k population: ${customdata[6]}<br>`

                document.getElementById('hoverinfo').innerHTML = text;
                document.getElementById('hoverinfo').style.visibility = 'visible';
            })

            gd.on('plotly_unhover', (e) => {
                document.getElementById('hoverinfo').style.visibility = 'hidden';
            });
		})
	});
}

function round(num, decimals=0){
    var k = Math.pow(10, decimals)
    return Math.round((num + Number.EPSILON) * k) / k
}

function splitTextInLines(text, wordsPerLine=5, lineSep='\n') {
    return [...Array(parseInt(text.split(' ').length/wordsPerLine + 1)).keys()].map(x=>text.split(' ').slice(x*wordsPerLine,(x+1)*wordsPerLine)).map(x=>x.join(' ')).join(lineSep)
}

function buildCustomData(id, covidData, provincesNames, popData) {
    return [
        provincesNames[id],
        round(popData[id].population),
        covidData[id].new_cases,
        round(100000*covidData[id].new_cases/popData[id].population, 2),
        covidData[id].active_cases_7,
        covidData[id].active_cases_14,
        round(100000*covidData[id].active_cases_14/popData[id].population, 2)
    ]
}

function create_data_for_map(locations, covidData, popData, provincesNames, provinciasGeojson) {
    return {
			type: "choroplethmapbox",
			locations: locations,
            z: locations.map(x=>round(100000*covidData[x].active_cases_14/popData[x].population, 2)),
            zmin: 0,
            zmax: 1000,
			customdata: locations.map(x=>buildCustomData(x, covidData, provincesNames, popData)),
			geojson: provinciasGeojson,
            hovertemplate: "%{customdata[0]}<extra></extra>",
			marker: {
				opacity: 0.6,
			},
			colorbar: {
				title: {
					text: "Incidence (14 days) per 100k population",
					side: "right"
				},
			},
			colorscale: [['0.0', colorBase], ['1.0', colorIncidence]], // "Reds"
    };
}


// INITIALIZATION OF THE MAP
// The width of the map is automatically responsive to the container div
// The height has to be specified here below, as Plotly doesn't support dynamic height.
createRiskMaps('map_incidence', 400);
