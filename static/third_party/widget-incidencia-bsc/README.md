<!-- ABOUT THE PROJECT -->
## Code structure
The package is formed for the next files:
- **cnig_provinces.geojson**: Store in a geojson the information about the province shapes for Spain
- **flowmaps-maps.js**: It contains the javascript functions for building the map. Firstly, makes a request to flowmaps API, retrieve the data, and build the map.
- **flowmaps-maps.css**: Contains basic styles for the maps and the *on hover* menu.
- **flowmaps-maps.html**: Contains an example on how this package can be used.

## Usage notes
After the package folder in your project, import the javascript and the css file into your template you want to use the maps on.
```
<script type="text/javascript" src="flowmaps-maps.js"></script>
<link rel="stylesheet" href="flowmaps-maps.css">
```

Once the files are imported, you can use the main function in order to generate the maps. The function is `createRiskMaps`, and has one parameter, the div where you want to build the map (string). For example, if you want to create the map widget on a div called "myDiv", you should, using javascript:
```
createRiskMaps("myDiv")
```
Check out the example `flowmaps-maps-html` for further details.

## Built With
The next libraries are used in order to build the maps:
* [Bootstrap](https://getbootstrap.com)
* [JQuery](https://jquery.com)
* [Plotly](https://plotly.com/javascript/)
