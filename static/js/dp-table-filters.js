const pipedCheckboxNames = [
  'year',
  'clinical_trial',
  'uni_or_multi',
  'topic_call',
]
function getCheckedValue(name) {
  const htmlCheckedInput = `input[name="${name}"]:checked`
  if (pipedCheckboxNames.includes(name)) {
    const nodeList = document.querySelectorAll(htmlCheckedInput).values()
    const checkedInputs = Array.from(nodeList)
    const pipedYears = checkedInputs.map(input => input.value).join('|')
    return pipedYears
  }
  return document.querySelector(htmlCheckedInput)?.value || ''
}
function applyFilters(table, columnIndices) {
  $('input:checkbox').on('change', (event) => {
    for (const [name, index] of Object.entries(columnIndices)) {
      const value = getCheckedValue(name)
      table.column(index).search(value, true, false, false).draw(false)
    }
  })
}