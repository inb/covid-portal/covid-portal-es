# ![COVID-19 Data Portal Spain](static/images/logos/cv19dp-logo-oneliner-es.png)

## COVID-19 Data Portal Spain

This is the source code for the Spanish Covid-19 Data Portal:
[https://www.covid19dataportal.es/](https://www.covid19dataportal.es/)

The website is the Spanish node for the European Covid-19 Data Portal project.
The main European site can be seen at [https://www.covid19dataportal.org/](https://www.covid19dataportal.org/)

- [Introduction](#introduction)
- [Implementation](#implementation)
  - [Hugo](#hugo)
  - [Docker](#docker)
    - [Development](#development)
    - [Staging (pre-production)](<#staging (pre-production)>)
    - [Production](#production)
- [How to get help](#how-to-get-help)
- [Credits](#credits)

## Introduction

This website is developed for [Instituto de Salud Carlos III](https://www.isciii.es/Paginas/Inicio.aspx) by the [Spanish National Bioinformatics Institute (INB)](https://inb-elixir.es/).

It is intended to provide a central place to provide information about:

- Available COVID-19 datasets
- Support services for COVID-19 researchers
- Information and support for publishing and sharing COVID-19 datasets

The site is built using the [Hugo](https://gohugo.io/) static web site generator.
It uses the [Bootstrap](https://getbootstrap.com/) and [DataTables](https://datatables.net/) frameworks.

The code of this site is a fork of the original code of the Swedish portal [https://www.covid19dataportal.se/](https://www.covid19dataportal.se/) and can be found in this [GitHub](https://github.com/ScilifelabDataCentre/covid-portal/tree/master) repository.

## Implementation

All website content is written in [Markdown](https://guides.github.com/features/mastering-markdown/), and so relatively easy to edit.

The code is hosted on [BSC Gitlab](https://gitlab.bsc.es/inb/covid-portal/covid-portal-es).

You can edit the website files on your computer in your favourite text editor. Just clone the repository to your machine:

```bash
git clone https://gitlab.bsc.es/inb/covid-portal/covid-portal-es.git
```

To make it easier to pull in changes made by developers, you can add the main repository as a remote:

```bash
git remote add upstream https://gitlab.bsc.es/inb/covid-portal/covid-portal-es.git
```

Then you can fetch changes at any time from this remote:

```bash
git pull upstream staging
```

### Hugo

To view your changes as they will appear in the final website, you need to install Hugo.
You can find instructions on the Hugo website: [https://gohugo.io/](https://gohugo.io/)

Once Hugo is installed, simply run the following command in the repository root directory:

```sh
hugo server
```

> Use the URL printed at the bottom of this message (here, it's `http://localhost:1313/`) to view the site.
> Every time you save a file, the page will automatically refresh in the browser.

### Docker

If you would prefer not to use Hugo, you can use the provided Dockerfile to build and run a container.

```sh
git pull --all
```

#### Development

```sh
git checkout <your-own-branch>
docker-compose up --build
```

#### Staging (pre-production)

```sh
git checkout staging
git pull origin staging
docker-compose -f staging.yml down
docker-compose -f staging.yml up --build -d
```

#### Production

```sh
git checkout master
git pull origin production
docker-compose -f production.yml down
docker-compose -f production.yml up --build -d
```

## How to get help

If in doubt, you can ask for help on email [covid19-dataportal@bsc.es](mailto:covid19-dataportal@bsc.es).

## Credits

The website was built by [INB/ELIXIR-ES](https://inb-elixir.es/)).
